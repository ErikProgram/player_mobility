﻿using UnityEngine;
using System.Collections;

public class playerAbility : MonoBehaviour
{
	public float speed = 6.0F;
	public float jumpHeight = 60.0F; 
	public float gravity = 20.0F;
	
	private Vector3 moveDirection = Vector3.zero;
	
	void Update()
	{
		CharacterController controller = GetComponent<CharacterController>();
		if (controller.isGrounded || reverseJump.onCeiling == 1)
		{
			moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
			moveDirection = transform.TransformDirection(moveDirection);
			moveDirection *= speed;
			if (Input.GetButton("Jump"))
			{
			moveDirection.y = jumpHeight;
			}

			if (Input.GetButton("Fall"))
			{
			moveDirection.y = -jumpHeight;
			}
		}
		moveDirection.y -= gravity * Time.deltaTime;
		controller.Move(moveDirection * Time.deltaTime);
	}
}